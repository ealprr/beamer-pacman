Pacman navigation bar for beamer
================================

Add a fancy pacman navigation bar to count your frame in your beamer presentation.
An exemple is available here: [demo slides](https://bitbucket.org/ealprr/beamer-pacman/downloads/pacman-sample.pdf)

The first version by serge-sans-paille can be found [here](http://serge.liyun.free.fr/serge/projects.2011-11-23.beamer-pacman.html).

![Example](https://bitbucket.org/ealprr/beamer-pacman/downloads/pacman-example.png)


## Usage

 1. copy `pacman.sty` in your beamer project folder
 2. add `\usepackage{pacman}` in the preamble

Tested with `pdflatex`, `Xelatex` and `lualatex`.


## Requirements

The package use: `tickz`, `pgf` and `hyperref`.


## Licence

The package is licensed under the terms of the BSD licence.

